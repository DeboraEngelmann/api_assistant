﻿using ApiAssistent.Application.Interfaces;
using ApiAssistent.Application.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace ApiAssistent.Application.Service
{
    public class StorageAppService : IStorageAppService
    {
        public readonly IConfiguration _configuration;
        public StorageAppService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<RetornoJson> CreateFileAsync(string plano, string problema)
        {
            Console.WriteLine("Aqui------" + _configuration["filePlano"]);
            var filePlano = _configuration["filePlano"];
            Console.WriteLine(filePlano);
            var pasta = _configuration["pasta"];
            var pathPlano = Path.Combine(Directory.GetCurrentDirectory(), pasta, filePlano);

            var fileRetono = _configuration["fileRetorno"];
            string pathRetorno = Path.Combine(Directory.GetCurrentDirectory(), pasta, fileRetono);

            if (File.Exists(pathRetorno))
            {
                File.Delete(pathRetorno);
            }
            if (File.Exists(pathPlano))
            {
                File.Delete(pathPlano);
            }



            if (!Directory.Exists(Path.GetDirectoryName(pathPlano)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(pathPlano));
            }

            using (StreamWriter writer = new StreamWriter(pathPlano, true))
            {
                await writer.WriteLineAsync(plano);
            }

            string fileProblema = _configuration["fileProblema"];
            string pathProblema = Path.Combine(Directory.GetCurrentDirectory(), pasta, fileProblema);
            if (File.Exists(pathProblema))
            {
                File.Delete(pathProblema);
            }

            using (StreamWriter writer = new StreamWriter(pathProblema, true))
            {
                await writer.WriteLineAsync(problema);
            }

            try
            {
                var process = new Process()
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = "ant",
                        Arguments = $"-e -f /home/debo/git/jason_assistant/bin/assistant.xml run Buildfile: /home/debo/git/jason_assistant/bin/assistant.xml",
                        RedirectStandardOutput = true,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                    }
                };

                process.Start();
                string result = process.StandardOutput.ReadToEnd();
                process.WaitForExit();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            while (!File.Exists(pathRetorno))
            {
                Console.WriteLine("Não Encontrou");
                await Task.Delay(1000);
            }
            string line = string.Empty;
            FileStream fileStream = new FileStream(pathRetorno, FileMode.Open);
            using (StreamReader reader = new StreamReader(fileStream))
            {
                line = reader.ReadToEnd();
            }

            var ret = JsonConvert.DeserializeObject<RetornoJson>(line);

            return ret;
        }
    }
}

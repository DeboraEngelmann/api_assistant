using System;
using System.Collections.Generic;

namespace ApiAssistent.Application.Models
{
    public class RetornoJson
    {
        public string Tipo { get; set; }
        public string Retorno { get; set; }
        public string Resultado { get; set; }
        public string Erro { get; set; }
        public IEnumerable<string> Reparo { get; set; }
    }
}
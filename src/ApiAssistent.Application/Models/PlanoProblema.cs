namespace ApiAssistent.Application.Models
{
    public class PlanoProblema
    {
        public string Plano { get; set; }
        public string Problema { get; set; }
    }
}
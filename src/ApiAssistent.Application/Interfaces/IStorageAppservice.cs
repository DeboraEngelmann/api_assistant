﻿using System.Threading.Tasks;
using ApiAssistent.Application.Models;

namespace ApiAssistent.Application.Interfaces
{
    public interface IStorageAppService
    {
        Task<RetornoJson> CreateFileAsync(string plano, string probmema);
    }
}

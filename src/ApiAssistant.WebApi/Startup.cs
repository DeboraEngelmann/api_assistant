﻿using ApiAssistant.CrossCutting.Filters;
using ApiAssistent.Application.Interfaces;
using ApiAssistent.Application.Service;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace ApiAssistant.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            // Configurando o serviço de documentação do Swagger  
            services.AddSwaggerDocumentation();

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();
            }));

            // Add Filters
            services.AddMvc(config =>
            {
                config.Filters.Add(new ExceptionFilter());
                config.Filters.Add(new ActionFilters());
            }).AddFluentValidation();

            services.AddSingleton<IFileProvider>(
              new PhysicalFileProvider(
                  Path.Combine(Directory.GetCurrentDirectory(), "/home/debo/git/api_assistant/src/ApiAssistant.WebApi/bin/Release/netcoreapp2.2/publish")));

            services.AddScoped<IStorageAppService, StorageAppService>();
 
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseDeveloperExceptionPage();
            app.UseCors("MyPolicy");
            app.UseSwaggerDocumentation();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiAssistant.WebApi
{
    public static class SwaggerServiceExtensions
    {
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0",
                    new Info
                    {
                        Version = "v1.0",
                        Title = "API Assistente de alocação de leitos",
                        Description = "API REST criada com o ASP.NET Core",
                        Contact = new Contact
                        {
                            Name = "Débora Cristina Engelmann",
                            Email = "debora.engelmann@edu.pucrs.br"

                        }
                    });
                c.DocumentFilter<SecurityRequirementsDocumentFilter>();

            });

            return services;
        }

        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Versioned API v1.0");
                c.SupportedSubmitMethods(SubmitMethod.Get, SubmitMethod.Post, SubmitMethod.Put, SubmitMethod.Patch, SubmitMethod.Options);
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "docs";

            });

            return app;
        }
    }
}

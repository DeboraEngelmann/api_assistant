﻿using ApiAssistent.Application.Interfaces;
using ApiAssistent.Application.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Buffers.Text;
using System.Text;
using System.Threading.Tasks;

namespace ApiAssistant.WebApi.Controllers
{
  
     [Route("api/[controller]")]
    [ApiController]
    public class TriggerValidatorController : ControllerBase
    {
        public readonly IStorageAppService _storageAppService;

        public TriggerValidatorController(IStorageAppService storageAppService)
        {
            _storageAppService = storageAppService;
        }


        // POST api/values
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Post([FromBody]PlanoProblema planoProblema)
        {
            var plano = Encoding.UTF8.GetString(Convert.FromBase64String(planoProblema.Plano));
            var problema = Encoding.UTF8.GetString(Convert.FromBase64String(planoProblema.Problema));
            var retorno = await _storageAppService.CreateFileAsync(plano, problema);

            return Ok(new
            {
                success = true,
                data = retorno
            });
        }



    }
}

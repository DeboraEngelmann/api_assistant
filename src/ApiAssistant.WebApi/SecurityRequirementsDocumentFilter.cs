﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace ApiAssistant.WebApi
{
    public class SecurityRequirementsDocumentFilter : IDocumentFilter
    {

        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            Dictionary<string, IEnumerable<string>> securityRequirements = new Dictionary<string, IEnumerable<string>>()
        {
            { "Bearer", new string[] { } }
        };

            swaggerDoc.Security = new[] { securityRequirements };
        }

    }
}
